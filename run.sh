 clear
 
# Clean up first
 pwd
 echo "Cleaning up old files..."
 find . -type f -name *beamer.pdf -print | xargs rm
 find . -type f -name *article.pdf -print | xargs rm
 find . -type f -name *.log -print | xargs rm
  
# slide creation
 cd 02beamers
 pwd
 for beamer in *.beamer.tex
 do
    echo "Processing $beamer..."
    pdflatex -halt-on-error $beamer 
    pdflatex -halt-on-error $beamer 
 done
 cd ..
  
# article creation
 cd 03articles
 pwd
 for article in *.article.tex
 do
    echo "Processing $article..."
#    pdflatex -halt-on-error $article 1>/dev/null
    pdflatex -halt-on-error $article
    pdflatex -halt-on-error $article  
 done
 cd ..

# consolidate chapters as one book
# rename -v 's/\.article\.pdf/\.pdf/' *.article.pdf
# echo "Consolidating chapters..."
# pdflatex -halt-on-error FundamentalsOfAndroid_StudentManual.tex 1> manual.log
 
# Move PDFs

echo "Moving PDFS..."
mv -v ./02beamers/*.pdf ./PDF/slides/
#mv -v ./03articles/FundamentalsOfAndroid_StudentManual.pdf ./PDF/
mv -v ./03articles/*.pdf ./PDF/handouts/

# clean up unneeded files
# echo "Final cleaning up of unneeded files..."
# cd ..
 find . -type f -name *.aux -print | xargs rm
 find . -type f -name *.nav -print | xargs rm
 find . -type f -name *.out -print | xargs rm
 find . -type f -name *.toc -print | xargs rm
 find . -type f -name *.snm -print | xargs rm
 find . -type f -name *.vrb -print | xargs rm
# find . -type f -name *.log -print | xargs rm


